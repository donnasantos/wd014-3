<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    public function index(){
        $tasks = Task::all();
        return view('tasks', compact('tasks'));
    }
}
