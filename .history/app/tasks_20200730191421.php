<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tasks;

class Tasks extends Model
{
    public function tasks(){
        return $this->hasMany('App\Tasks');
        
    }  
}
