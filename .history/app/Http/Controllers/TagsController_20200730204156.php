<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index(){
        $tags = Tags::all();
        return view('tags', compact('tags'));
    }

    public function create(){
        return view('add-tag');
    }

    public function store(Request $request){
        $new_tags = new Tasks;
        $new_tags->name = $request->name;
        $new_tags->save();
        return redirect('/tags');
    }

    public function destroy(Request $request){
        $id = $request->tags_id;
        $tag = Tags::find($id);
        $tag->delete();
        return redirect('/tags');
    }

    public function edit(Request $request){
        $task = Tasks::find($id);
        return view('update-tags', compact('update tag'));
    }

    public function update($id, Request $request){
        $task = Tasks::find($id);
        $task->title = $request->title;
        $task->save();
        return redirect ('/tasks');
    }
}
