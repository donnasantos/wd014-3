<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;

class TaskController extends Controller
{
    public function index(){
        $tasks = Tasks::all();
        return view('tasks', compact('tasks'));
    }

    public function create(){
        return view('add-task');
    }

    public function store(Request $request){
        $new_tasks = new Task;
        $new_tasks->title = $request->title;
        $new_tasks->save();
        return redirect('/tasks');
    }

    public function destroy(Request $request){
        $id = $request->task_id;
        $task = Bloc::find($id);
        $task->delete();
        return redirect('/tasks');
    }
}
