<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;

class Tasks extends Model
{
    public function index(){
        $tasks = Task::all();
        return view('tasks', compact('tasks'));
    }

    public function create(){
        return view('add-task');
    }

    public function store(Request $request){
        $new_tasks = new Task;
        $new_tasks->title = $request->title;
        $new_tasks->save();
        return redirect('/tasks');
    }
}
