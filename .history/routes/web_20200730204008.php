<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tasks', 'TaskController@index');
Route::get('add-task', 'TaskController@create');
Route::post('/add-task', 'TaskController@store');
Route::delete('/delete-task', 'TaskController@destroy');
Route::get('/update-task/{id}', 'TaskController@edit');
Route::patch('/update-task/{id}', 'TaskController@update');

Route::get('/tags', 'TagsController@index');
Route::get('add-tag', 'TagsController@create');
Route::post('/add-tag', 'TagsController@store');
Route::delete('/delete-tag', 'TagsController@destroy');
Route::get('/update-tag/{id}', 'TagsController@edit');
Route::patch('/update-tag/{id}', 'TagsController@update');