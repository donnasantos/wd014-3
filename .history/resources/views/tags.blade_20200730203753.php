@extends('layouts.template')
@section('title', 'Tasks')
@section('content')

<h1 class="py-5 text-center">Tasks</h1>
    <div class="text-center">
        <a href="/add-task" class="btn btn-primary">Add Tags</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="hidden">Tag ID</th>
                            <th>Tag:</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td class="hidden">{{ $task->id }}</td>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->description }}</td>
                                <td>{{ $task->status_id }}</td>
                                <td class="d-flex">
                                    <form action="/delete-task" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="task_id" value="{{ $task->id }}">
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <a href="/update-task/{{ $task->id }}" class="btn btn-info">Update</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection