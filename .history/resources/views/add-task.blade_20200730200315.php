@extends('layouts.template');
@section('title', 'Add Task');
@section('content')
    <h1 class="py-5 text-center">Add Task</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-task" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Task Name</label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <!-- <div class="form-group">
                        <label for="body">Description</label>
                        <input type="text" name="body" class="form-control">
                    </div>
                   -->
                    <!-- <div class="form-group">
                        <label for="status_id">Status:</label>
                            <select name="status_id" class="form-control">
                                    <option value="{{$status->id}}">{{status->name}}</option>
                                    @endforeach
                                </select>
                            <div class="text-center">
                    </div> -->
                        <button type="submit" class="btn btn-primary">Add Task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection