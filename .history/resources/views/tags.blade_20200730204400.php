@extends('layouts.template')
@section('title', 'Tags')
@section('content')

<h1 class="py-5 text-center">Tags</h1>
    <div class="text-center">
        <a href="/add-tag" class="btn btn-primary">Add Tags</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="hidden">Tag ID</th>
                            <th>Tag:</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tags as $tag)
                            <tr>
                                <td class="hidden">{{ $tag->id }}</td>
                                <td>{{ $tag->name }}</td>
                                <td></td>
                            
                                <td class="d-flex">
                                    <form action="/delete-tag" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="tag_id" value="{{ $tag->id }}">
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <a href="/update-tag/{{ $tag->id }}" class="btn btn-info">Update</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection