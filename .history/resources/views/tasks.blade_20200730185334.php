@extends('layouts.template')
@section('title', 'Tasks')
@section('content')

<h1 class="py-5 text-center">Tasks</h1>
    <div class="text-center">
        <a href="/add-task" class="btn btn-primary">Add Task</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="hidden">Task ID:</th>
                            <th>Task:</th>
                            <th>Description:</th>
                            <th>Status:</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td class="hidden">{{$tasks->id}}</td>
                                <td>{{$tasks->title}}</td>
                                <td>{{$tasks->body}}</td>
                                <td>{{$student->status->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection