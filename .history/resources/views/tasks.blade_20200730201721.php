@extends('layouts.template')
@section('title', 'Tasks')
@section('content')

<h1 class="py-5 text-center">Tasks</h1>
    <div class="text-center">
        <a href="/add-task" class="btn btn-primary">Add Task</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="hidden">Task ID:</th>
                            <th>Task:</th>
                            <th>Description:</th>
                            <th>Status:</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td class="hidden">{{ $task->id }}</td>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->body }}</td>
                                <td>{{ $task->status_id }}</td>
                                <td class="d-flex">
                                    <form action="/task-bloc" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="bloc_id" value="{{ $task->id }}">
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <a href="/update-bloc/{{ $task->id }}" class="btn btn-info">Update</a>
                                    <a href="/show-bloc/{{ $bloc->id }}" class="btn btn-danger">Show Bloc Details</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection