@extends('layouts.template')
@section('title', 'Add Task')
@section('content')
    <h1 class="text-center py-5">Add Task</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-student" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="full_name">Student Name:</label>
                        <input type="text" class="form-control" name="full_name">
                    </div>
                    <div class="form-group">
                        <label for="bloc_id">Bloc:</label>
                        <select name="bloc_id" class="form-control">
{{--                            In here, I want to publish all the blocs in my db, in a dropdown--}}
                            @foreach($blocs as $bloc)
                                <option value="{{$bloc->id}}">{{$bloc->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-info">Add Student</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
