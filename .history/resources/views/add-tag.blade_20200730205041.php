@extends('layouts.template');
@section('title', 'Add Tag');
@section('content')
    <h1 class="py-5 text-center">Add Tag</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-tag" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Tag Name</label>
                        <input type="text" name="title" class="form-control">
                    </div>
                 
                    
                        <button type="submit" class="btn btn-primary">Add Tag</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection