<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;

class TaskController extends Controller
{
    public function index(){
        $tasks = Tasks::all();
        return view('tasks', compact('tasks'));
    }

    public function create(){
        return view('add-task');
    }

    public function store(Request $request){
        $new_tasks = new Tasks;
        $new_tasks->title = $request->title;
        $new_tasks->description = $request->description;
        $new_tasks->status_id = $request->status_id;
        $new_tasks->save();
        return redirect('/tasks');
    }

    public function destroy(Request $request){
        $id = $request->task_id;
        $task = Tasks::find($id);
        $task->delete();
        return redirect('/tasks');
    }

    public function edit(Request $request){
        $task = Tasks::find($id);
        return view('update-task', compact('update task'));
    }

    public function update($id, Request $request){
        $task = Tasks::find($id);
        $task->title = $request->title;
        $task->save();
        return redirect ('/tasks');
    }
}
